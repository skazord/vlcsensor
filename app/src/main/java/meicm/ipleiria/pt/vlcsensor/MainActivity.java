package meicm.ipleiria.pt.vlcsensor;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.ParcelUuid;
import android.os.PowerManager;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

public class MainActivity extends Activity implements View.OnClickListener, ServiceFragment.ServiceFragmentDelegate {

    // Bluetooth
    static int REQUEST_ENABLE_BT = 1;
    static int REQUEST_FINE_LOCATION = 2;
    static int REQUEST_BT_PERMISSION = 3;

    BluetoothAdapter mBluetoothAdapter;
    private BluetoothGattService mBluetoothGattService;
    private BluetoothGattCharacteristic mMovementCharacteristic;
    private HashSet<BluetoothDevice> mBluetoothDevices;

    private BluetoothManager mBluetoothManager;
    private AdvertiseData mAdvData;
    private AdvertiseData mAdvScanResponse;
    private AdvertiseSettings mAdvSettings;
    private BluetoothLeAdvertiser mAdvertiser;
    private ServiceFragment mCurrentServiceFragment;
    //private ServiceFragment.ServiceFragmentDelegate mDelegate;

    public HashMap mScanResults;
    public Handler mHandler;

    boolean mAdvertise = false;

    UUID VLC_SENSOR_SERVICE_UUID = UUID
            .fromString("0000120D-0000-1000-8000-00805f9b34fb");//convertFromInteger(0x120D);
    UUID VLC_SENSOR_MOVEMENT_UUID = UUID
            .fromString("00002E37-0000-1000-8000-00805f9b34fb"); //convertFromInteger(0x2E37);

    private static final String VLC_SENSOR_DESCRIPTION = "Movimento enviado por BLE para controlar o VLC.";

    private static final UUID CHARACTERISTIC_USER_DESCRIPTION_UUID = UUID
            .fromString("00002901-0000-1000-8000-00805f9b34fb");
    private static final UUID CLIENT_CHARACTERISTIC_CONFIGURATION_UUID = UUID
            .fromString("00002902-0000-1000-8000-00805f9b34fb");

    int VLC_MOVEMENT_TOGGLE_POSITIVE = 0;
    int VLC_MOVEMENT_TOGGLE_NEGATIVE = 1;
    int VLC_MOVEMENT_VOLUME_POSITIVE = 2;
    int VLC_MOVEMENT_VOLUME_NEGATIVE = 3;
    int VLC_MOVEMENT_TRACK_POSITIVE= 4;
    int VLC_MOVEMENT_TRACK_NEGATIVE = 5;

    TextView mAdvStatus;
    TextView mConnectionStatus;
    TextView tvMacAddress;
    Button advertiseButton;

    // Settings
    float minAccelTrigger = 3.5f;
    float minGyroTrigger = 1.5f;

    // Sensors
    private SensorView mSensorView;
    private SensorManager mSensorManager;
    private PowerManager mPowerManager;
    private WindowManager mWindowManager;
    private Display mDisplay;
    private PowerManager.WakeLock mWakeLock;
    TextView tvX;
    TextView tvY;
    TextView tvZ;
    TextView tvX2;
    TextView tvY2;
    TextView tvZ2;
    TextView tvGyro;
    TextView tvCounter;
    TextView tvAction;

    long currentTimer = 0;
    long lastChange = 0;
    int counter = 0;

    long currentTimerVolume = 0;
    //long lastChangeVolume = 0;

    long currentTimerTracks = 0;

    private final AdvertiseCallback mAdvCallback = new AdvertiseCallback() {
        @Override
        public void onStartFailure(int errorCode) {
            super.onStartFailure(errorCode);
            int statusText;
            switch (errorCode) {
                case ADVERTISE_FAILED_ALREADY_STARTED:
                    statusText = ADVERTISE_FAILED_ALREADY_STARTED;
                    break;
                case ADVERTISE_FAILED_DATA_TOO_LARGE:
                    statusText = ADVERTISE_FAILED_DATA_TOO_LARGE;
                    break;
                case ADVERTISE_FAILED_FEATURE_UNSUPPORTED:
                    statusText = ADVERTISE_FAILED_FEATURE_UNSUPPORTED;
                    break;
                case ADVERTISE_FAILED_INTERNAL_ERROR:
                    statusText = ADVERTISE_FAILED_INTERNAL_ERROR;
                    break;
                case ADVERTISE_FAILED_TOO_MANY_ADVERTISERS:
                    statusText = ADVERTISE_FAILED_TOO_MANY_ADVERTISERS;
                    break;
                default:
                    statusText = 999;
            }
            mAdvStatus.setText(statusText);
        }

        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            super.onStartSuccess(settingsInEffect);
            mAdvStatus.setText("Advertise Successful");
        }
    };

    private BluetoothGattServer mGattServer;
    private final BluetoothGattServerCallback mGattServerCallback = new BluetoothGattServerCallback() {
        @Override
        public void onConnectionStateChange(BluetoothDevice device, final int status, int newState) {
            super.onConnectionStateChange(device, status, newState);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothGatt.STATE_CONNECTED) {
                    mBluetoothDevices.add(device);
                    updateConnectedDevicesStatus();
                    System.out.println("Connected to device: " + device.getAddress());
                } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                    mBluetoothDevices.remove(device);
                    updateConnectedDevicesStatus();
                    System.out.println("Disconnected from device");
                }
            } else {
                mBluetoothDevices.remove(device);
                updateConnectedDevicesStatus();
                // There are too many gatt errors (some of them not even in the documentation) so we just
                // show the error to the user.
                final String errorMessage = "Error when connecting: " + status;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                    }
                });
                System.out.println("Error when connecting: " + status);
            }
        }

        @Override
        public void onCharacteristicReadRequest(BluetoothDevice device, int requestId, int offset,
                                                BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic);
            System.out.println("Device tried to read characteristic: " + characteristic.getUuid());
            System.out.println("Value: " + Arrays.toString(characteristic.getValue()));
            if (offset != 0) {
                mGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_INVALID_OFFSET, offset,
                        /* value (optional) */ null);
                return;
            }
            mGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS,
                    offset, characteristic.getValue());
        }

        @Override
        public void onNotificationSent(BluetoothDevice device, int status) {
            super.onNotificationSent(device, status);
            System.out.println("Notification sent. Status: " + status);
        }

        @Override
        public void onCharacteristicWriteRequest(BluetoothDevice device, int requestId,
                                                 BluetoothGattCharacteristic characteristic, boolean preparedWrite, boolean responseNeeded,
                                                 int offset, byte[] value) {
            super.onCharacteristicWriteRequest(device, requestId, characteristic, preparedWrite,
                    responseNeeded, offset, value);
            System.out.println("Characteristic Write request: " + Arrays.toString(value));
            int status = mCurrentServiceFragment.writeCharacteristic(characteristic, offset, value);
            if (responseNeeded) {
                mGattServer.sendResponse(device, requestId, status,
                        /* No need to respond with an offset */ 0,
                        /* No need to respond with a value */ null);
            }
        }

        @Override
        public void onDescriptorReadRequest(BluetoothDevice device, int requestId,
                                            int offset, BluetoothGattDescriptor descriptor) {
            super.onDescriptorReadRequest(device, requestId, offset, descriptor);
            System.out.println("Device tried to read descriptor: " + descriptor.getUuid());
            System.out.println("Value: " + Arrays.toString(descriptor.getValue()));
            if (offset != 0) {
                mGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_INVALID_OFFSET, offset,
                        /* value (optional) */ null);
                return;
            }
            mGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset,
                    descriptor.getValue());
        }

        @Override
        public void onDescriptorWriteRequest(BluetoothDevice device, int requestId,
                                             BluetoothGattDescriptor descriptor, boolean preparedWrite, boolean responseNeeded,
                                             int offset,
                                             byte[] value) {
            super.onDescriptorWriteRequest(device, requestId, descriptor, preparedWrite, responseNeeded,
                    offset, value);
            System.out.println("Descriptor Write Request " + descriptor.getUuid() + " " + Arrays.toString(value));
            int status = BluetoothGatt.GATT_SUCCESS;
            if (descriptor.getUuid() == VLC_SENSOR_SERVICE_UUID) {
                BluetoothGattCharacteristic characteristic = descriptor.getCharacteristic();
                boolean supportsNotifications = (characteristic.getProperties() &
                        BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0;
                boolean supportsIndications = (characteristic.getProperties() &
                        BluetoothGattCharacteristic.PROPERTY_INDICATE) != 0;

                if (!(supportsNotifications || supportsIndications)) {
                    status = BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED;
                } else if (value.length != 2) {
                    status = BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH;
                } else if (Arrays.equals(value, BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE)) {
                    status = BluetoothGatt.GATT_SUCCESS;
                    mCurrentServiceFragment.notificationsDisabled(characteristic);
                    descriptor.setValue(value);
                } else if (supportsNotifications &&
                        Arrays.equals(value, BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)) {
                    status = BluetoothGatt.GATT_SUCCESS;
                    mCurrentServiceFragment.notificationsEnabled(characteristic, false /* indicate */);
                    descriptor.setValue(value);
                } else if (supportsIndications &&
                        Arrays.equals(value, BluetoothGattDescriptor.ENABLE_INDICATION_VALUE)) {
                    status = BluetoothGatt.GATT_SUCCESS;
                    mCurrentServiceFragment.notificationsEnabled(characteristic, true /* indicate */);
                    descriptor.setValue(value);
                } else {
                    status = BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED;
                }
            } else {
                status = BluetoothGatt.GATT_SUCCESS;
                descriptor.setValue(value);
            }
            if (responseNeeded) {
                mGattServer.sendResponse(device, requestId, status,
                        /* No need to respond with offset */ 0,
                        /* No need to respond with a value */ null);
            }
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // SENSORS
        tvX = (TextView) findViewById(R.id.textViewX);
        tvY = (TextView) findViewById(R.id.textViewY);
        tvZ = (TextView) findViewById(R.id.textViewZ);
        tvX2 = (TextView) findViewById(R.id.textViewX2);
        tvY2 = (TextView) findViewById(R.id.textViewY2);
        tvZ2 = (TextView) findViewById(R.id.textViewZ2);
        tvGyro = (TextView) findViewById(R.id.textViewGyro);
        tvCounter = (TextView) findViewById(R.id.textViewCounter);
        tvAction = (TextView) findViewById(R.id.textViewAction);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        mDisplay = mWindowManager.getDefaultDisplay();
        mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, getClass().getName());

        mSensorView = new SensorView(this);

        // BLE
        mAdvStatus = (TextView) findViewById(R.id.textView_advertisingStatus);
        tvMacAddress = (TextView) findViewById(R.id.textView_macaddress);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mAdvStatus = (TextView) findViewById(R.id.textView_advertisingStatus);
        mConnectionStatus = (TextView) findViewById(R.id.textView_connectionStatus);
        mBluetoothDevices = new HashSet<>();
        mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();

        //mDelegate = (ServiceFragment.ServiceFragmentDelegate) this;

        mAdvSettings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                .setConnectable(true)
                .build();
        mAdvData = new AdvertiseData.Builder()
                .setIncludeTxPowerLevel(true)
                .addServiceUuid(getServiceUUID())
                .build();
        mAdvScanResponse = new AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .build();

        mMovementCharacteristic = new BluetoothGattCharacteristic(VLC_SENSOR_MOVEMENT_UUID,
                BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                BluetoothGattCharacteristic.PERMISSION_READ);

        mMovementCharacteristic.addDescriptor(
                getClientCharacteristicConfigurationDescriptor());

        mMovementCharacteristic.addDescriptor(
                getCharacteristicUserDescriptionDescriptor(VLC_SENSOR_DESCRIPTION));

        mBluetoothGattService = new BluetoothGattService(VLC_SENSOR_SERVICE_UUID,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);
        mBluetoothGattService.addCharacteristic(mMovementCharacteristic);

        advertiseButton = findViewById(R.id.button_advertise);
        advertiseButton.setOnClickListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        resetStatusViews();
        // If the user disabled Bluetooth when the app was in the background,
        // openGattServer() will return null.
        mGattServer = mBluetoothManager.openGattServer(this, mGattServerCallback);
        if (mGattServer == null) {
            ensureBleFeaturesAvailable();
            return;
        }
        // Add a service for a total of three services (Generic Attribute and Generic Access
        // are present by default).
        mGattServer.addService(mBluetoothGattService);

        if (mBluetoothAdapter.isMultipleAdvertisementSupported()) {
            mAdvertiser = mBluetoothAdapter.getBluetoothLeAdvertiser();
            mAdvertiser.startAdvertising(mAdvSettings, mAdvData, mAdvScanResponse, mAdvCallback);
        } else {
            mAdvStatus.setText("This device is not BLE capable");
        }
    }

    @Override
    protected void onResume(){
        super.onResume();

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "This device is not BLE capable", Toast.LENGTH_SHORT);
            finish();
        }

        mWakeLock.acquire();
        mSensorView.startReading();
    }

    @Override
    protected void onPause(){
        super.onPause();

        mSensorView.stopReading();

        mWakeLock.release();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGattServer != null) {
            mGattServer.close();
        }
        if (mBluetoothAdapter.isEnabled() && mAdvertiser != null) {
            // If stopAdvertising() gets called before close() a null
            // pointer exception is raised.
            mAdvertiser.stopAdvertising(mAdvCallback);
        }
        resetStatusViews();
    }

    public ParcelUuid getServiceUUID() {
        return new ParcelUuid(VLC_SENSOR_SERVICE_UUID);
    }

    @Override
    public void sendNotificationToDevices(BluetoothGattCharacteristic characteristic) {
        boolean indicate = (characteristic.getProperties()
                & BluetoothGattCharacteristic.PROPERTY_INDICATE)
                == BluetoothGattCharacteristic.PROPERTY_INDICATE;
        for (BluetoothDevice device : mBluetoothDevices) {
            // true for indication (acknowledge) and false for notification (unacknowledge).
            mGattServer.notifyCharacteristicChanged(device, characteristic, indicate);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.button_advertise: {
                if(mAdvertise){
                    mAdvertise = false;
                    advertiseButton.setText("Start Advertising");
                    Toast.makeText(this, "Stopped Advertising", Toast.LENGTH_SHORT);
                }else{
                    mAdvertise = true;
                    advertiseButton.setText("Stop Advertising");
                    Toast.makeText(this, "Started Advertising", Toast.LENGTH_SHORT);
                }
            }
        }
    }

    private void resetStatusViews() {
        mAdvStatus.setText("Not advertising");
        updateConnectedDevicesStatus();
    }

    private void updateConnectedDevicesStatus() {
        final String message = "Devices connected "
                + mBluetoothManager.getConnectedDevices(BluetoothGattServer.GATT).size();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionStatus.setText(message);
            }
        });
    }

    private void ensureBleFeaturesAvailable() {
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "BLE not supported by this device", Toast.LENGTH_LONG).show();
            finish();
        } else if (!mBluetoothAdapter.isEnabled()) {
            // Make sure bluetooth is enabled.
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    private void disconnectFromDevices() {
        System.out.println("Disconnecting devices...");
        for (BluetoothDevice device : mBluetoothManager.getConnectedDevices(
                BluetoothGattServer.GATT)) {
            System.out.println("Devices: " + device.getAddress() + " " + device.getName());
            mGattServer.cancelConnection(device);
        }
    }

    class SensorView extends FrameLayout implements SensorEventListener {

         private Sensor mLinearAccelerometer;
         private Sensor mOrientation;
         private Sensor mGyroscope;

         private float mSensorX = 0;
         private float mSensorY = 0;
         private float mSensorZ = 0;

        private float mSensorOrientX = 0;
        private float mSensorOrientY = 0;
        private float mSensorOrientZ = 0;

        private float mSensorGyroX = 0;
        private float mSensorGyroY = 0;
        private float mSensorGyroZ = 0;
        private boolean mSensorGyroReset = false;

         public void startReading(){
             mSensorManager.registerListener(this, mLinearAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
             mSensorManager.registerListener(this, mOrientation, SensorManager.SENSOR_DELAY_NORMAL);
             mSensorManager.registerListener(this, mGyroscope, SensorManager.SENSOR_DELAY_NORMAL);
         }

         public void stopReading() {
             mSensorManager.unregisterListener(this);
         }

         public SensorView(Context context){
             super(context);
             mLinearAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
             mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
             mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

         }

         @Override
        public void onSensorChanged(SensorEvent event){

             if(event.sensor.getType() == Sensor.TYPE_ORIENTATION) {
                 mSensorOrientX = event.values[0];
                 mSensorOrientY = event.values[1];
                 mSensorOrientZ = event.values[2];
                 tvX2.setText("X:"+String.valueOf((int)mSensorOrientX));
                 tvY2.setText("Y:"+String.valueOf((int)mSensorOrientY));
                 tvZ2.setText("Z:"+String.valueOf((int)mSensorOrientZ));

                 // Movimento YZ Mudar faixas
                 if(event.values[2] < -45.f && System.currentTimeMillis() - currentTimerTracks > 1000){
                     currentTimerTracks = System.currentTimeMillis();
                     tvAction.setText("Movimento Track >>");
                     counter++;
                     tvCounter.setText("Contador: " + counter);
                     advertiseMovement(VLC_MOVEMENT_TRACK_POSITIVE);
                 }
                 if(event.values[2] > 45.f && System.currentTimeMillis() - currentTimerTracks > 1000){
                     currentTimerTracks = System.currentTimeMillis();
                     tvAction.setText("Movimento Track <<");
                     counter++;
                     tvCounter.setText("Contador: " + counter);
                     advertiseMovement(VLC_MOVEMENT_TRACK_NEGATIVE);
                 }

             }

             if(event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION){
                 mSensorX = event.values[0];
                 mSensorY = event.values[1];
                 mSensorZ = event.values[2];
                 tvX.setText("X:"+String.valueOf(String.format("%.2f", mSensorX)));
                 tvY.setText("Y:"+String.valueOf(String.format("%.2f", mSensorY)));
                 tvZ.setText("Z:"+String.valueOf(String.format("%.2f", mSensorZ)));

                 // EIXO Y
                 if((mSensorOrientY < 40.f && mSensorOrientY > 0.f) || (mSensorOrientY > -40.f && mSensorOrientY < 0.f)){
                     // Movimento Y para a frente
                     if(System.currentTimeMillis() - currentTimer < 2000 && event.values[1] < -minAccelTrigger){
                         tvAction.setText("Movimento Frontal detectado");
                         currentTimer = 0;
                         lastChange = System.currentTimeMillis();
                         counter++;
                         tvCounter.setText("Contador: " + counter);
                         advertiseMovement(VLC_MOVEMENT_TOGGLE_POSITIVE);
                     }

                     // Movimento Y para trás
                     if(System.currentTimeMillis() - currentTimer < 2000 && event.values[1] > minAccelTrigger){
                         tvAction.setText("Movimento Traseiro detectado");
                         currentTimer = 0;
                         lastChange = System.currentTimeMillis();
                         counter++;
                         tvCounter.setText("Contador: " + counter);
                         advertiseMovement(VLC_MOVEMENT_TOGGLE_NEGATIVE);
                     }

                     if(event.values[1] > minAccelTrigger && (System.currentTimeMillis() - lastChange > 1000)){
                         currentTimer = System.currentTimeMillis();
                     }

                     if(event.values[1] < -minAccelTrigger && (System.currentTimeMillis() - lastChange > 1000)){
                         currentTimer = System.currentTimeMillis();
                     }
                 }

                 // EIXO Z
                 if((mSensorOrientY > 50.f && mSensorOrientY < 100.f) || (mSensorOrientY < -50.f && mSensorOrientY > -100.f)){
                     // Movimento Y para a frente
                     if(System.currentTimeMillis() - currentTimer < 2000 && event.values[2] < -minAccelTrigger){
                         tvAction.setText("Movimento Frontal detectado");
                         currentTimer = 0;
                         lastChange = System.currentTimeMillis();
                         counter++;
                         tvCounter.setText("Contador: " + counter);
                         advertiseMovement(VLC_MOVEMENT_TOGGLE_POSITIVE);
                     }

                     // Movimento Y para trás
                     if(System.currentTimeMillis() - currentTimer < 2000 && event.values[2] > minAccelTrigger){
                         tvAction.setText("Movimento Traseiro detectado");
                         currentTimer = 0;
                         lastChange = System.currentTimeMillis();
                         counter++;
                         tvCounter.setText("Contador: " + counter);
                         advertiseMovement(VLC_MOVEMENT_TOGGLE_NEGATIVE);
                     }

                     if(event.values[2] > minAccelTrigger && (System.currentTimeMillis() - lastChange > 1000)){
                         currentTimer = System.currentTimeMillis();
                     }

                     if(event.values[2] < -minAccelTrigger && (System.currentTimeMillis() - lastChange > 1000)){
                         currentTimer = System.currentTimeMillis();
                     }
                 }

             }

             if(event.sensor.getType() == Sensor.TYPE_GYROSCOPE){
                 String gyro = "X: " + String.valueOf(String.format("%.2f", event.values[0])) + " / Y: "
                         + String.valueOf(String.format("%.2f", event.values[1])) +
                         " / Z: " + String.valueOf(String.format("%.2f", event.values[2]));
                 tvGyro.setText(gyro);

                 mSensorGyroX = event.values[0];
                 mSensorGyroY = event.values[1];
                 mSensorGyroZ = event.values[2];

                 if(mSensorGyroZ > minGyroTrigger && ((mSensorOrientZ > -10 && mSensorOrientZ < 10) && (mSensorOrientY > -10 && mSensorOrientY < 10))){
                     if(System.currentTimeMillis() - currentTimerVolume > 1000){
                         currentTimerVolume = System.currentTimeMillis();
                         counter++;
                         tvCounter.setText("Contador: " + counter);
                         tvAction.setText("Movimento Volume --");
                         mSensorGyroReset = true;
                         advertiseMovement(VLC_MOVEMENT_VOLUME_NEGATIVE);
                     }
                 }

                 if(mSensorGyroZ < -minGyroTrigger && ((mSensorOrientZ > -10 && mSensorOrientZ < 10) && (mSensorOrientY > -10 && mSensorOrientY < 10))){
                     if(System.currentTimeMillis() - currentTimerVolume > 1000) {
                         currentTimerVolume = System.currentTimeMillis();
                         counter++;
                         tvCounter.setText("Contador: " + counter);
                         tvAction.setText("Movimento Volume ++");
                         mSensorGyroReset = true;
                         advertiseMovement(VLC_MOVEMENT_VOLUME_POSITIVE);
                     }
                 }
             }

         }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy){

        }

    }

    // BLE PERIPHERAL

    private void advertiseMovement(int movementId){
        if(mAdvertise){
            mMovementCharacteristic.setValue(movementId,
                    BluetoothGattCharacteristic.FORMAT_UINT8, 0);

            sendNotificationToDevices(mMovementCharacteristic);
        }
    }

    public static BluetoothGattDescriptor getClientCharacteristicConfigurationDescriptor() {
        BluetoothGattDescriptor descriptor = new BluetoothGattDescriptor(
                CLIENT_CHARACTERISTIC_CONFIGURATION_UUID,
                (BluetoothGattDescriptor.PERMISSION_READ | BluetoothGattDescriptor.PERMISSION_WRITE));
        descriptor.setValue(new byte[]{0, 0});
        return descriptor;
    }

    public static BluetoothGattDescriptor getCharacteristicUserDescriptionDescriptor(String defaultValue) {
        BluetoothGattDescriptor descriptor = new BluetoothGattDescriptor(
                CHARACTERISTIC_USER_DESCRIPTION_UUID,
                (BluetoothGattDescriptor.PERMISSION_READ | BluetoothGattDescriptor.PERMISSION_WRITE));
        try {
            descriptor.setValue(defaultValue.getBytes("UTF-8"));
        } finally {
            return descriptor;
        }
    }

    private class BleScanCallback extends ScanCallback {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            addScanResult(result);
        }
        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult result : results) {
                addScanResult(result);
            }
        }
        @Override
        public void onScanFailed(int errorCode) {
            // fail scan
            System.out.println("Scan failed: " + errorCode);
        }
        private void addScanResult(ScanResult result) {
            BluetoothDevice device = result.getDevice();
            String deviceAddress = device.getAddress();

            mScanResults.put(deviceAddress, device);
        }
    };

    private boolean hasPermissions() {
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            requestBluetoothEnable();
            return false;
        } else if(!hasBluetoothPermissions()){
            requestBluetoothPermission();
            return false;
        } else if (!hasLocationPermissions()) {
            requestLocationPermission();
            return false;
        }
        return true;
    }
    private void requestBluetoothEnable() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }
    private boolean hasLocationPermissions() {
        return checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
    private boolean hasBluetoothPermissions() {
        return checkSelfPermission(Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestBluetoothPermission() {
        requestPermissions(new String[]{Manifest.permission.BLUETOOTH}, REQUEST_BT_PERMISSION);
    }
    private void requestLocationPermission() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_FINE_LOCATION);
    }

}
